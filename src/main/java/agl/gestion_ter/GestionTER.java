package agl.gestion_ter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GestionTER {
	//attributs
	private ArrayList<Groupe> groupes;
	private ArrayList<Sujet> sujets;
	
	//constructeur
	public GestionTER() {
	  this.groupes = new ArrayList<>();
	  this.sujets = new ArrayList<>();
	}
	
	//getters and setters
	public void addGroupe(String nom) {
	  this.groupes.add(new Groupe(nom));
	}
	public void addSujet(String titre) {
	  this.sujets.add(new Sujet(titre));
	}
	public List<Sujet> getSujets() {
	  return this.sujets;
	}
	public int getNbGroupe() {
	  return this.groupes.size();
	}
	public Groupe getGroupe(int id) {
	  if(id >= 0 && id < this.getNbGroupe()) {
	    return this.groupes.get(id);
	  }
	  return new Groupe("n'existe pas");
	}
	
	//méthodes
	/**
	* toString
	* @return String instance bien formée
	*/
	public String toString() {
	  StringBuilder str = new StringBuilder();
	  str.append("Liste des groupes:\n----------------\n");
	  for(int i=0; i<this.groupes.size(); i++) {
	    str.append(this.groupes.get(i).toString() + "\n");
	  }
	  str.append("\n\nListe des sujets:\n------------------\n");
	  for(int i=0; i<this.sujets.size(); i++) {
	    str.append(this.sujets.get(i).toString() + "\n");
	  }
	  return str.toString();
	}
	/**
	* serialize les groupes
	* enregistre le résultat dans target/groupes.json
	*/
	public void serializeGroupe() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    test.writeValue(new File("target/groupes.json"), this.groupes);
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	/**
	* serialize les groupes
	* enregistre le résultat dans target/sujets.json
	*/
	public void serializeSujet() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    test.writeValue(new File("target/sujets.json"), this.sujets);
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	/**
	* importe les groupes depuis target/groupes.json
	* les deserialize et écrase les groupes actuels dans gestionTer
	*/
	public void importGroupe() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    Groupe[] groupesS = test.readValue(new File("target/groupes.json"), Groupe[].class);
	    this.groupes = new ArrayList<>();
	    for(int i=0; i<groupesS.length; i++) {
	      this.groupes.add(groupesS[i]);
	    }
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
	/**
	* importe les sujets depuis target/sujets.json
	* les deserialize et écrase les groupes actuels dans gestionTer
	*/
	public void importSujet() {
	  ObjectMapper test = new ObjectMapper();
	  try {
	    Sujet[] sujetsS = test.readValue(new File("target/sujets.json"), Sujet[].class);
	    this.sujets = new ArrayList<>();
	    for(int i=0; i<sujetsS.length; i++) {
	      this.sujets.add(sujetsS[i]);
	    }
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	}
}